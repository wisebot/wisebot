#include <ZumoMotors.h>
#include <Sensors.h>

#define L_RAN_PIN A0
#define R_RAN_PIN A1

ZumoMotors motors;
SharpIRAnalog l_analog(L_RAN_PIN, 25, 93);
SharpIRAnalog r_analog(R_RAN_PIN, 25, 93);

const uint8_t sensorThreshold = 3;
const uint16_t turnSpeedMax = 400;
const uint16_t turnSpeedMin = 100;
const uint16_t acceleration = 10;
const uint16_t deceleration = 10;

#define LEFT 0
#define RIGHT 1

bool senseDir = RIGHT;
bool turningLeft = false;
bool turningRight = false;
uint16_t turnSpeed = turnSpeedMax;

void setup()
{
  pinMode(L_RAN_PIN, INPUT);
  pinMode(R_RAN_PIN, INPUT);
  delay(500);
  Serial.begin(9600);
  delay(1000);
  
  motors.flipRightMotor(true);
}

void turnRight()
{
  motors.setLeftSpeed(turnSpeed);
  motors.setRightSpeed(-turnSpeed);
  turningLeft = false;
  turningRight = true;
}

void turnLeft()
{
  motors.setLeftSpeed(-turnSpeed);
  motors.setRightSpeed(turnSpeed);
  turningLeft = true;
  turningRight = false;
}

void stop()
{
  motors.setLeftSpeed(0);
  motors.setRightSpeed(0);
  turningLeft = false;
  turningRight = false;
}

void loop()
{
  int l_range,
      r_range;
  
  //read distances
  l_range = l_analog.distance();
  r_range = r_analog.distance();
/*  l_range = constrain(l_range, 4, 30);
  r_range = constrain(r_range, 4, 30);*/

  bool seen = l_range < 30 || r_range < 30;
  
  if(seen)
  {
    turnSpeed -= deceleration;
  }
  else
  {
    turnSpeed += acceleration;
  }
  
  turnSpeed = constrain(turnSpeed, turnSpeedMin, turnSpeedMax);
  
  if(seen)
  { 
    bool lastTurn = turnRight;
    
    if((r_range - l_range) >= sensorThreshold)
    {
      turnRight();
      senseDir = RIGHT;
    }
    else if((l_range - r_range) >= sensorThreshold)
    {
      turnLeft();
      senseDir = LEFT;
    }
    else
    {
      stop();
    }
  }
  else
  {
    if(senseDir == RIGHT)
    {
      turnRight();
    }
    else
    {
      turnLeft();
    }
  }
/*
  Serial.print(l_range);
  Serial.print('\t');
  Serial.print(analogRead(L_RAN_PIN));
  Serial.print('\t');
  Serial.print(r_range);
  Serial.print('\t');
  Serial.print(analogRead(R_RAN_PIN));
  Serial.print('\t');
  Serial.println();

  delay(250);
*/
}
