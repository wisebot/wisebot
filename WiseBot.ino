#include <QTRSensors.h>
#include <ZumoMotors.h>
#include <defs.h>
#include <Actions.h>
#include <Tracks.h>
#include <Sensors.h>
#include <ActionManager.h>

void setup() {
  GetTracks( )->Setup();
  GetSensors()->Setup();
}

void loop() {
  GetSensors()->TakeData();
  GetActionManager()->ChooseAction();
  GetActionManager()->DoAction();
  GetTracks( )->Update();
}
