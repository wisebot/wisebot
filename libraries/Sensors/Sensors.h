#ifndef SENSORS_H_
#define SENSORS_H_

#include <defs.h>
#include "QTRSensors.h"

#define NUM_LINE_SENSORS  2

enum TargetSide {
  TargetSide_Left,
  TargetSide_Right
};

enum LineSensors {
  LineSensors_Left,
  LineSensors_Right,
  LineSensors_Both
};

////////////////////
class SharpIRAnalog {
 public:
  SharpIRAnalog( int pin, int avg, int tolerance );
  int distance();

 private:
  int cm();

  int _pin;
  int _avg;
  int _tol;
  int _p;
  int _sum;
  int _prevDist;
};

class SideDigSensors {
 public:
  SideDigSensors( int l_pin, int r_pin , int f_pin );
  bool left();
  bool right();
	bool forward();

 private:
  int _l_pin;
  int _r_pin;
	int _f_pin;
};
//////////////////

class Sensors {
public:

  bool OnLine();
  bool TargetForward();
  bool TargetLongDistance();
  bool TargetShortDistance();
  TargetSide GetTargetLastSide();
  LineSensors LastLineSensorActive();
  bool TargetInLeftAnalog();
  bool TargetInRightAnalog();
  bool Working();

  void TakeData();
  void Setup();

private:
	// данные с датчиков
	struct SENSORS_DATA
	{
		unsigned int SensorLineVal[NUM_LINE_SENSORS];
		int left_dist;
		int right_dist;

		SharpIRAnalog left_analog;
		SharpIRAnalog right_analog;
		SideDigSensors digital;

		SENSORS_DATA( )
		: left_dist( 0 )
		, right_dist( 0 )
		, left_analog( L_RAN_PIN, 25, 93 )
		, right_analog( R_RAN_PIN , 25 , 93 )
		, digital( L_D_RAN_PIN, R_D_RAN_PIN , F_D_RAN_PIN )
		{

		}
	} data;
};

Sensors *GetSensors();

#endif // SENSORS_H_
