#include <Sensors.h>
#include <Arduino.h>
#include <defs.h>

#define EMITTER_PIN  QTR_NO_EMITTER_PIN
#define TIMEOUT      2500

// Функция считывания датчиков линии
QTRSensorsRC qtrrc((unsigned char[]) {2,4},
  NUM_LINE_SENSORS, TIMEOUT, EMITTER_PIN);

Sensors *GetSensors() {
  static Sensors sensors;
  return &sensors;
}

static LineSensors last_line_sensor = LineSensors_Both;
static TargetSide last_target_side = TargetSide_Left;

bool Sensors::OnLine() {
	if ( data.SensorLineVal[0] < 1500 || data.SensorLineVal[1] < 1500 )
		return true;

  return false;
}

bool Sensors::TargetForward() {
	return data.digital.forward();
  //return TargetInLeftAnalog() && TargetInRightAnalog();
}

bool Sensors::TargetLongDistance() {
  if ( !TargetForward( ) )
		return false;

	return data.right_dist >= 10;
}

bool Sensors::TargetShortDistance() {
	if ( !TargetForward( ) )
		return false;

	return data.right_dist < 10;
}

TargetSide Sensors::GetTargetLastSide() {
  return last_target_side;
}

LineSensors Sensors::LastLineSensorActive() {
  return last_line_sensor;
}

bool Sensors::TargetInLeftAnalog() {
  return data.left_dist < MAX_ANALOG_DIST;
}

bool Sensors::TargetInRightAnalog() {
  return data.right_dist < MAX_ANALOG_DIST;
}

void Sensors::TakeData() {
	// Считывание датчиков линии
	qtrrc.read( data.SensorLineVal );
	if ( data.SensorLineVal[0] < 1500 && data.SensorLineVal[1] < 1500 )
		last_line_sensor = LineSensors_Both;
	else if ( data.SensorLineVal[0] < 1500 )
		last_line_sensor = LineSensors_Left;
	else if ( data.SensorLineVal[1] < 1500 )
		last_line_sensor = LineSensors_Right;

	// Считывание аналоговых датчиков
	data.left_dist = data.left_analog.distance();
	data.right_dist = data.right_analog.distance();

	// Считывание цифровых датчиков
	if ( data.digital.left() || TargetInLeftAnalog() )
		last_target_side = TargetSide_Right;
	else if ( data.digital.right() || TargetInRightAnalog() )
		last_target_side = TargetSide_Left;
	/*
	if ( TargetInLeftAnalog() )
		last_target_side = TargetSide_Left;
	else if ( TargetInRightAnalog() )
		last_target_side = TargetSide_Right;
*/
}

void Sensors::Setup() {
  pinMode(L_RAN_PIN, INPUT);
  pinMode(R_RAN_PIN, INPUT);
  pinMode(L_D_RAN_PIN, INPUT);
  pinMode(R_D_RAN_PIN, INPUT);
}

bool Sensors::Working() {
  return digitalRead( START_MATCH_PIN );
}

///////////////////////////////////////////////////////////////////////////////

SideDigSensors::SideDigSensors(int l_pin, int r_pin , int f_pin ) {
  _l_pin=l_pin;
  _r_pin=r_pin;
	_f_pin = f_pin;
}

bool SideDigSensors::left() {
  bool flag = digitalRead( _l_pin );
  return !flag;
}

bool SideDigSensors::right() {
  bool flag = digitalRead( _r_pin );
  return !flag;
}

bool SideDigSensors::forward() {
	bool flag = digitalRead( _f_pin );
	return !flag;
}

SharpIRAnalog::SharpIRAnalog(int pin, int avg, int tolerance) {
  _pin=pin;
  _avg=avg;
  _tol=tolerance;
}

int SharpIRAnalog::cm() {
  int raw = analogRead( _pin );
  raw = constrain( raw, 81, 614);
  //  float volts = raw*0.0048828125;   // value from sensor * (5/1024)
  int dist = (12 * 1024) / (raw * 5);          // worked out from graph 65 = theretical distance / (1/Volts)S
  dist = constrain( dist, 4, 30 );

  return dist;
}

int SharpIRAnalog::distance() {
  _p = 0;
  _sum = 0;
  _prevDist = 0;

  for( int i = 0; i < _avg; ++i)
    {
      int tmp = cm();

      if( tmp >= ( _prevDist * _tol / 100 ) )
	{
	  _prevDist = tmp;
	  _sum = _sum + tmp;
	  _p++;
	}
    }

  if( _p != 0 )
    {
      int accurateDist = _sum / _p;
      return accurateDist;
    }
  else
    return -1;
}
