#ifndef ACTIONS_H_
#define ACTIONS_H_

class Actions {
public:
  Actions();

  void SearchTarget();
  void AttackLongDistance();
  void AttackShortDistance();
  void ReturnOnRing( bool reset );
  void AimingTarget();
  bool OnRing() const;

private:

  enum TimingsMs {
    TimingsMs_ReturnStageGoBack = 120,
    TimingsMs_ReturnStageRotate = 500 + TimingsMs_ReturnStageGoBack
  };

  unsigned long returnOnRingTime_;
  bool onRing_;

};

Actions *GetActions();

#endif // ACTIONS_H_
