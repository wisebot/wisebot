#include <Actions.h>
#include <Tracks.h>
#include <Sensors.h>
#include <ActionManager.h>

Actions *GetActions() {
  static Actions actions;
  return &actions;
}

Actions::Actions() : returnOnRingTime_( 0 ), onRing_( true ) {
}

bool Actions::OnRing() const {
  return onRing_;
}

void Actions::SearchTarget() {
  const TargetSide targetLastSide = GetSensors()->GetTargetLastSide();

  switch( targetLastSide ) {
    case TargetSide_Left:
      GetTracks()->TurnLeft();
      break;

    case TargetSide_Right:
      GetTracks()->TurnRight();
      break;
  }
}

void Actions::AttackLongDistance() {
  AimingTarget();
  GetTracks()->MoveForwardFast();
}

void Actions::AttackShortDistance() {
  AimingTarget();
  GetTracks()->MoveForwardFast();
}

void Actions::ReturnOnRing( bool reset ) {
  if( reset )
    returnOnRingTime_ = millis();

  onRing_ = false;
  const unsigned long timeExpired = millis() - returnOnRingTime_;

  if( timeExpired <= TimingsMs_ReturnStageGoBack ) {

    GetTracks()->MoveBackward();

  } /*else if( timeExpired <= TimingsMs_ReturnStageRotate ) {

    const LineSensors lineSensors = GetSensors()->LastLineSensorActive();
    switch( lineSensors ) {
      case LineSensors_Left:
        GetTracks()->TurnRight();
        break;

      case LineSensors_Right:
        GetTracks()->TurnLeft();
        break;

      case LineSensors_Both:
        // Без разницы куда крутить, поэтому направо.
        GetTracks()->TurnRight();
        break;
    }

  }*/else {
    onRing_ = true;
  }
}

void Actions::AimingTarget() {
  const bool left = GetSensors()->TargetInLeftAnalog();
  const bool right = GetSensors()->TargetInRightAnalog();

  if( left && right )
    return;

  if( left )
    GetTracks()->TurnLeft();
  else if( right )
    GetTracks()->TurnRight();
}
