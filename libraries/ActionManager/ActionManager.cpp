#include <ActionManager.h>
#include <Sensors.h>
#include <Actions.h>

ActionManager *GetActionManager() {
  static ActionManager actionManager;
  return &actionManager;
}

void ActionManager::ChooseAction() {
  if( !GetSensors()->Working() ) {
    currentAction_ = ActionType_DoNothing;
    return;
  }

  if( GetSensors()->OnLine() )
    currentAction_ = ActionType_ReturnOnRingBegin;
  else if( GetActions()->OnRing() == false )
    currentAction_ = ActionType_ReturnOnRingContinue;
  else {

    if( GetSensors()->TargetForward() == false )
      currentAction_ = ActionType_SearchTarget;

    if( GetSensors()->TargetLongDistance() )
      currentAction_ = ActionType_AttackLongDist;

    if( GetSensors()->TargetShortDistance() )
      currentAction_ = ActionType_AttackShortDist;
  }
}

void ActionManager::DoAction() {
  switch( currentAction_ ) {
    case ActionType_AttackLongDist:
      GetActions()->AttackLongDistance();
      break;

    case ActionType_AttackShortDist:
      GetActions()->AttackShortDistance();
      break;

    case ActionType_SearchTarget:
      GetActions()->SearchTarget();
      break;

    case ActionType_ReturnOnRingBegin:
      GetActions()->ReturnOnRing( true );
      break;

    case ActionType_ReturnOnRingContinue:
      GetActions()->ReturnOnRing( false );
      break;

    default:
      break;
  }
}
