#ifndef ACTIONMANAGER_H_
#define ACTIONMANAGER_H_

enum ActionType {
  ActionType_SearchTarget,
  ActionType_ReturnOnRingBegin,
  ActionType_ReturnOnRingContinue,
  ActionType_AttackLongDist,
  ActionType_AttackShortDist,
  ActionType_DoNothing
};

class ActionManager {
public:
  ActionManager() {
  }

  void ChooseAction();
  void DoAction();

private:

  ActionType currentAction_;
};

ActionManager *GetActionManager();

#endif // ACTIONMANAGER_H_
