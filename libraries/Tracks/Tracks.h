#ifndef TRACKS_H
#define TRACKS_H

#include "ZumoMotors.h"

// Класс для работы с движками (гусеницами)
class Tracks
{
public:
	Tracks( ) {};
	~Tracks( ) {};
	
	// Тест
	void Test( );

	// Настройка портов для движков
	void Setup( );

	// Повернуть налево
	void TurnLeft( );

	// Повернуть направо
	void TurnRight( );

	// Ехать вперед с крейсерской скоростью
	void MoveForwardCruise( );

	// Ехать вперед с максимальной скоростью
	void MoveForwardFast( );

	// Ехать назад
	void MoveBackward( );

	// Остановка
	void Stop( );
	
	// Применение действий к движкам
	void Update( );
private:
	struct TRACK_STATE
	{
		// Направление поворота
		// -1 налево
		// 0 никуда не поворачиваем
		// 1 направо
		int dir;

		// Направление тяги:
		// -1 назад
		// 0 стоим
		// 1 крейсерский вперед
		// 2 полный вперед
		int forward;

		int16_t speed_right;	// текущая скорость на правом движке
		int16_t speed_left;		// текущая скорость на левом движке

		TRACK_STATE()
		: dir( 0 )
		, forward( 0 )
		, speed_right( 0 )
		, speed_left( 0 )
		{}
	} state;
	
	// Обновление смещения скорости движков
	void UpdateShiftSpeed( );

	// Расчет скорости при движении назад
	void CalcBackSpeed( );

	// Расчет скорости, когда стоим
	void CalcStopSpeed( );

	// Расчет скорости при крейсерском движении
	void CalcCruiseSpeed( );

	// Расчет скорости при максимальной скорости
	void CalcMaxSpeed( );

	// Переинициализация состояния движков
	void ResetState( );
};

// Получение экземпляра класса для работы с движками (гусеницами)
Tracks* GetTracks();

#endif TRACKS_H
