#include "Tracks.h"
#include "defs.h"

ZumoMotors motors;

namespace
{
	const int16_t SpeedMax = 400;	// максимальная скорость
	const int16_t SpeedMin = -400;	// минимальная скорость

	// добавочная разность на один движок при повороте
	const int16_t SpeedShift = 20;

	// крейсерская скорость
	const int16_t SpeedCruising = 300;

	// Предыдущее направление поворота
	int prev_dir = 0;
	// Предыдущий режим движения
	int prev_forward = 0;
	// Накрученная разность скорости на левом движке (во время поворота)
	int16_t LeftShiftSpeed = 0;
	// Накрученная разность скорости на правом движке (во время поворота)
	int16_t RightShiftSpeed = 0;
	
	// максимальная скорость смещения
	const int16_t MaxShiftSpeed = 400;
}

Tracks* GetTracks( )
{
	static Tracks single_track;
	return &single_track;
}

void Tracks::Setup( )
{
	motors.flipRightMotor(true);
}

void Tracks::TurnLeft( )
{
	state.dir = -1;
}

void Tracks::TurnRight( )
{
	state.dir = 1;
}

void Tracks::MoveForwardCruise( )
{
	state.forward = 1;
}

void Tracks::MoveForwardFast( )
{
	state.forward = 2;
}

void Tracks::MoveBackward( )
{
	state.forward = -1;
}

void Tracks::Stop( )
{
	state.forward = 0;
}

void Tracks::UpdateShiftSpeed( )
{
	if ( prev_dir != state.dir || prev_forward != state.forward )
	{
		LeftShiftSpeed = 0;
		RightShiftSpeed = 0;
	}

	if ( state.dir == -1 )
	{
		LeftShiftSpeed -= SpeedShift;
		if ( state.forward == 0 )
		{
			RightShiftSpeed += SpeedShift;
		}
	}
	else if ( state.dir == 1 )
	{
		RightShiftSpeed -= SpeedShift;
		if ( state.forward == 0 )
		{
			LeftShiftSpeed += SpeedShift;
		}
	}
	else
	{
		LeftShiftSpeed = 0;
		RightShiftSpeed = 0;
	}

	LeftShiftSpeed = constrain( LeftShiftSpeed , -MaxShiftSpeed , MaxShiftSpeed );
	RightShiftSpeed = constrain( RightShiftSpeed , -MaxShiftSpeed , MaxShiftSpeed );
}

void Tracks::CalcBackSpeed( )
{
	if ( state.dir == 0 )
	{
		state.speed_left = SpeedMin;
		state.speed_right = SpeedMin;
	}
	else if ( state.dir == -1 )
	{
		state.speed_left = SpeedMin - LeftShiftSpeed;
		state.speed_right = SpeedMin;
	}
	else if ( state.dir == 1 )
	{
		state.speed_left = SpeedMin;
		state.speed_right = SpeedMin - RightShiftSpeed;
	}
}

void Tracks::CalcStopSpeed( )
{
	if ( state.dir == 0 )
	{
		state.speed_left = 0;
		state.speed_right = 0;
	}
	else if ( state.dir == -1 )
	{
		state.speed_left = LeftShiftSpeed;
		state.speed_right = RightShiftSpeed;
	}
	else if ( state.dir == 1 )
	{
		state.speed_left = LeftShiftSpeed;
		state.speed_right = RightShiftSpeed;
	}
}

void Tracks::CalcCruiseSpeed( )
{
	if ( state.dir == 0 )
	{
		state.speed_left = SpeedCruising;
		state.speed_right = SpeedCruising;
	}
	else if ( state.dir == -1 )
	{
		state.speed_left = SpeedCruising + LeftShiftSpeed;
		state.speed_right = SpeedCruising;
	}
	else if ( state.dir == 1 )
	{
		state.speed_left = SpeedCruising;
		state.speed_right = SpeedCruising + RightShiftSpeed;
	}
}

void Tracks::CalcMaxSpeed( )
{
	if ( state.dir == 0 )
	{
		state.speed_left = SpeedMax;
		state.speed_right = SpeedMax;
	}
	else if ( state.dir == -1 )
	{
		state.speed_left = SpeedMax + LeftShiftSpeed;
		state.speed_right = SpeedMax;
	}
	else if ( state.dir == 1 )
	{
		state.speed_left = SpeedMax;
		state.speed_right = SpeedMax + RightShiftSpeed;
	}
}

void Tracks::Update( )
{
	// Расчет разницы скоростей при повороте
	UpdateShiftSpeed( );

	// Расчет абсолютных скоростей
	if ( state.forward == -1 )
	{
		CalcBackSpeed( );
	}
	else if ( state.forward == 0 )
	{
		CalcStopSpeed( );
	}
	else if ( state.forward == 1 )
	{
		CalcCruiseSpeed( );
	}
	else if ( state.forward == 2 )
	{
		CalcMaxSpeed( );
	}

	// Применение изменений
	state.speed_right = constrain( state.speed_right , SpeedMin , SpeedMax );
	state.speed_left = constrain( state.speed_left , SpeedMin , SpeedMax );
	motors.setLeftSpeed( state.speed_right );
	motors.setRightSpeed( state.speed_left );

/*
	Serial.print( "state.speed_right\t" );
	Serial.print( state.speed_right );
	Serial.println();
	Serial.print( "state.speed_left\t" );
	Serial.print( state.speed_left );
	Serial.println();
	Serial.println();
*/
	// Обнуление состояния движков
	ResetState( );
}

void Tracks::ResetState( )
{
	prev_dir = state.dir;
	prev_forward = state.forward;
	
	state.dir = 0;
	state.forward = 0;
	state.speed_right = 0;
	state.speed_left = 0;
}


///////////////////////////////////////////////////////////////////

void Tracks::Test( )
{
	static unsigned int time = millis( );
	const unsigned int action_time = 1000;

	// Стоим
	static bool mission_complete_1 = false;
	if ( !mission_complete_1 )
	{
		if ( millis( ) - time < action_time )
		{
			Stop( );
			return;
		}
		time = millis( );
		mission_complete_1 = true;
	}

	// Поворот на месте налево
	static bool mission_complete_2 = false;
	if ( !mission_complete_2 )
	{
		if ( millis( ) - time < action_time )
		{
			TurnLeft( );
			return;
		}
		time = millis( );
		mission_complete_2 = true;
	}

	// Поворот на месте направо
	static bool mission_complete_3 = false;
	if ( !mission_complete_3 )
	{
		if ( millis( ) - time < action_time )
		{
			TurnRight( );
			return;
		}
		time = millis( );
		mission_complete_3 = true;
	}

	// Назад
	static bool mission_complete_4 = false;
	if ( !mission_complete_4 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveBackward( );
			return;
		}
		time = millis( );
		mission_complete_4 = true;
	}

	// Назад и налево
	static bool mission_complete_5 = false;
	if ( !mission_complete_5 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveBackward( );
			TurnLeft( );
			return;
		}
		time = millis( );
		mission_complete_5 = true;
	}

	// Назад и направо
	static bool mission_complete_6 = false;
	if ( !mission_complete_6 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveBackward( );
			TurnRight( );
			return;
		}
		time = millis( );
		mission_complete_6 = true;
	}

	// Крейсерский вперед
	static bool mission_complete_7 = false;
	if ( !mission_complete_7 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardCruise( );
			return;
		}
		time = millis( );
		mission_complete_7 = true;
	}

	// Крейсерский вперед налево
	static bool mission_complete_8 = false;
	if ( !mission_complete_8 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardCruise( );
			TurnLeft( );
			return;
		}
		time = millis( );
		mission_complete_8 = true;
	}
	
	// Крейсерский вперед направо
	static bool mission_complete_9 = false;
	if ( !mission_complete_9 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardCruise( );
			TurnRight( );
			return;
		}
		time = millis( );
		mission_complete_9 = true;
	}

	// Максимальный вперед
	static bool mission_complete_10 = false;
	if ( !mission_complete_10 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardFast( );
			return;
		}
		time = millis( );
		mission_complete_10 = true;
	}

	// Максимальный вперед налево
	static bool mission_complete_11 = false;
	if ( !mission_complete_11 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardFast( );
			TurnLeft( );
			return;
		}
		time = millis( );
		mission_complete_11 = true;
	}

	// Максимальный вперед направо
	static bool mission_complete_12 = false;
	if ( !mission_complete_12 )
	{
		if ( millis( ) - time < action_time )
		{
			MoveForwardFast( );
			TurnRight( );
			return;
		}
		time = millis( );
		mission_complete_12 = true;
	}

	// Остановиться
	Stop( );
}
