#ifndef DEFS_H
#define DEFS_H

#include <QTRSensors.h>
#include <ZumoMotors.h>

#define L_RAN_PIN A1
#define R_RAN_PIN A2
#define MAX_ANALOG_DIST	30

#define L_D_RAN_PIN 13
#define R_D_RAN_PIN 12
#define F_D_RAN_PIN 6

const int START_MATCH_PIN = 11;

#endif DEFS_H
