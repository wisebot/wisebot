#include <QTRSensors.h>
#include <ZumoMotors.h>

#define NUM_SENSORS  2
#define TIMEOUT      2500
#define EMITTER_PIN  QTR_NO_EMITTER_PIN

#define LEFT 1
#define RIGHT 0

QTRSensorsRC qtrrc((unsigned char[]) {2,4},
  NUM_SENSORS, TIMEOUT, EMITTER_PIN);
unsigned int sensorValues[NUM_SENSORS];

ZumoMotors motors;
const uint16_t SpeedCruse = 300;
const uint16_t SpeedMax = 400;

void setup()
{
  delay(500);
  Serial.begin(9600);
  delay(1000);
  motors.flipRightMotor(true);
}

void turn(bool DIR)
{
  if(DIR)
  {
    motors.setLeftSpeed(-SpeedMax);
    motors.setRightSpeed(SpeedMax);
  }
  else
  {
    motors.setLeftSpeed(SpeedMax);
    motors.setRightSpeed(-SpeedMax);
  }
  delay(180);
}

void stop()
{
  motors.setLeftSpeed(0);
  motors.setRightSpeed(0);
}

void go()
{
  motors.setLeftSpeed(SpeedCruse);
  motors.setRightSpeed(SpeedCruse);
}

void back()
{
  motors.setLeftSpeed(-SpeedCruse);
  motors.setRightSpeed(-SpeedCruse);
  delay(130);
}

void loop() {
  qtrrc.read(sensorValues);
/*
  for(unsigned char i = 0; i < NUM_SENSORS; i++)
  {
    Serial.print(sensorValues[i]);
    Serial.print('\t');
  }
  Serial.println();

  delay(250);
*/
  if(sensorValues[0] < 1500)
  {
    stop();
    back();
    turn(RIGHT);
  }
  else if(sensorValues[1] < 1500)
  {
    stop();
    back();
    turn(LEFT);
  }
  else
  {
    go();
  }
}
